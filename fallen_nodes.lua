--[[

   Nodes Crumble fallen_nodes.lua

   Copyright 2017 Hamlet <h4mlet@riseup.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

]]--


local MODPATH = minetest.get_modpath(minetest.get_current_modname())


--
-- Fallen Nodes module support
--

local DIRT = minetest.get_content_id"default:dirt"
local DIRT_GRASS = minetest.get_content_id"default:dirt_with_grass"
local DIRT_GRASS_FOOTSTEPS =
   minetest.get_content_id"default:dirt_with_grass_footsteps"

local DIRT_GRASS_DRY = minetest.get_content_id"default:dirt_with_dry_grass"
local DIRT_GRASS_SNOW = minetest.get_content_id"default:dirt_with_snow"
local DIRT_FOREST_LITTER =
   minetest.get_content_id"default:dirt_with_rainforest_litter"

--
-- Voxel Manipulator
--

minetest.register_on_generated(function()

   local vm, emin, emax = minetest.get_mapgen_object"voxelmanip"
   local data = vm:get_data()
   local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}

   for i in area:iterp(emin, emax) do

      if (data[i] == DIRT) or
         (data[i] == DIRT_GRASS) or
         (data[i] == DIRT_GRASS_FOOTSTEPS) or
         (data[i] == DIRT_GRASS_DRY) or
         (data[i] == DIRT_GRASS_SNOW) or
         (data[i] == DIRT_FOREST_LITTER) then

         local pos = area:position(i)

         minetest.check_for_falling(pos)

      end
   end
end)

if minetest.get_modpath("darkage") then
   dofile(MODPATH .. "/darkage.lua")
end

if minetest.get_modpath("mg") then
   dofile(MODPATH .. "/mg.lua")
end
