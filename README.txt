
   NODES CRUMBLE
   =============

   Nodes set to falling_node will crumble on map generation.


   Version: 0.1.0
   Source code's license: GPLv3

   Dependencies: default (found in minetest_game)
   Supported: darkage (Addi's fork), fallen_nodes, mg (Experimental Mapgen)


   Installation
   ------------
   
   Unzip the archive, rename the folder to nodes_crumble and place it
   in minetest/mods/

   (  GNU/Linux: If you use a system-wide installation place
      it in ~/.minetest/mods/  )

   (  If you only want this to be used in a single world, place
      the folder in worldmods/ in your worlddirectory.  )

   For further information or help see:
      http://wiki.minetest.net/wiki/Installing_Mods
