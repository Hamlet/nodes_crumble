--[[

   Nodes Crumble init.lua

   Copyright 2017 Hamlet <h4mlet@riseup.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

]]--


local MODPATH = minetest.get_modpath(minetest.get_current_modname())


--
-- Minetest Game's default falling nodes
--

local SAND = minetest.get_content_id"default:sand"
local SAND_DESERT = minetest.get_content_id"default:desert_sand"
local SAND_SILVER = minetest.get_content_id"default:silver_sand"

local GRAVEL = minetest.get_content_id"default:gravel"
local SNOW = minetest.get_content_id"default:snow"


--
-- Voxel Manipulator
--

minetest.register_on_generated(function()

   local vm, emin, emax = minetest.get_mapgen_object"voxelmanip"
   local data = vm:get_data()
   local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}

   for i in area:iterp(emin, emax) do

      if (data[i] == SAND) or
         (data[i] == SAND_DESERT) or
         (data[i] == SAND_SILVER) or
         (data[i] == GRAVEL) or
         (data[i] == SNOW) then

         local pos = area:position(i)

         minetest.check_for_falling(pos)

      end
   end
end)


if minetest.get_modpath("fallen_nodes") then
   dofile(MODPATH .. "/fallen_nodes.lua")
end
